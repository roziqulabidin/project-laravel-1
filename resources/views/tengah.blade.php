<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FORM HTML</title>
</head>
<body>
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <p>First Name:</p>
    <input type="text" name="fn">
    <p>Last Name:</p>
    <input type="text" name="ln">
    <p>Gender:</p>
    <input type="radio" name="gnd" id="male">Male
    <br>
    <input type="radio" name="gnd" id="female">Female
    <p>Nationality:</p>
    <select name="Ntn">
        <option value="ind">WNI</option>
        <option value="oind">WNA</option>
    </select>
    <p>Language Spoken:</p>
    <input type="checkbox" name="indonesian">Indonesian
    <br>
    <input type="checkbox" name="english">English
    <br>
    <input type="checkbox" name="others">Others
    <p>Bio:</p>
    <textarea name="" id="" cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value="Submit">
</form>  
</body>
</html>